import { createMuiTheme } from "@material-ui/core";
import { ThemeOptions } from "@material-ui/core/styles/createMuiTheme";
import { themeConfig, typography, variantMapping, themeFonts } from "./config";
import "./src/style/fonts.css";

function createTheme(options: ThemeOptions) {
  return createMuiTheme({
    spacing: themeConfig.spacing,
    typography: {
      themeFontStyles: themeConfig.typography.themeFontStyles,
      fontFamily: themeFonts.primary,
      allVariants: {},
      body1: {
        ...typography("body1"),
      },
      body2: {
        ...typography("body2"),
      },
      h1: {
        ...typography("h1"),
      },
      h2: {
        ...typography("h2"),
      },
      h3: {
        ...typography("h3"),
      },
      h4: {
        ...typography("h4"),
      },
      h5: {
        ...typography("h5"),
      },
      h6: {
        ...typography("h6"),
      },
      button: { ...typography("button") },
      subtitle1: {
        ...typography("subtitle1"),
      },
      subtitle2: {
        ...typography("subtitle2"),
      },
      overline: {
        ...typography("overline"),
      },
      caption: {
        ...typography("caption"),
      },
    },
    palette: {
      ...themeConfig.palette,

      editionBall: {
        color: "#e9550d",
        backgroundColor: "#ffc583",
        bottom: 12,
      },

      error: {
        main: "#B94A48",
      },
    },
    shape: {
      borderRadius: 0,
    },
    props: {
      MuiCard: {
        elevation: 0,
      },
      MuiExpansionPanel: {
        elevation: 0,
      },
      MuiInputLabel: {
        shrink: true,
      },
      MuiTypography: {
        variant: "body2",
        // variantMapping: variantMapping,
      },
    },
    overrides: {
      MuiTypography: {
        paragraph: {
          marginBottom: "1.66em",
        },
        gutterBottom: {
          marginBottom: "0.5em",
        },
      },
      MuiButton: {
        outlined: {
          borderRadius: 50,
          paddingLeft: 36,
          paddingRight: 36,
        },
      },
      MuiLink: {
        root: {
          color: "inherit",
        },
      },
    },
    ...(options as any),
  });
}

const themeBondig = createTheme({});

export default themeBondig;
