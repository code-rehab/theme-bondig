import createSpacing from "@material-ui/core/styles/createSpacing";
import { TypographyStyleOptions } from "@material-ui/core/styles/createTypography";

// FONTS /////////////////////////////////////
export const themeFonts: any = {
  primary: "Domaine",
  secondary: "Domaine",
  tertiary: "Domaine",
};

// FONTSTYLES ///////////////////////////////
export const themeFontStyles: any = [
  {
    component: "h6",
    text: "Koptekst Groot",
    variant: "h1",
  },
  {
    component: "h1",
    text: "Koptekst",
    variant: "h1",
  },
  {
    component: "h2",
    text: "Subtitel",
    variant: "h2",
  },
  {
    component: "h3",
    text: "Introtekst",
    variant: "h3",
  },
  {
    component: "h4",
    text: "Sub-subtitel",
    variant: "h4",
  },
  {
    component: "h5",
    text: "Bijschrift",
    variant: "h5",
  },
  {
    component: "body1",
    text: "Paragraaf",
    variant: "body1",
  },
];

// COLORS ///////////////////////////////////
export const themeColors: any = {
  primary: {
    main: "#F85B00",
    light: "#FFECE1",
  },
  secondary: {
    main: "#A1D7DD",
    light: "#EDF7F9",
  },
  text: {
    main: "#4a4a4a",
  },
  white: "#fff",
};

// COLORPICKER ///////////////////////////////
export const colorSet: any = [
  themeColors.primary.main,
  themeColors.primary.light,
  themeColors.secondary.main,
  themeColors.secondary.light,
  themeColors.text.main,
  // themeColors.white,
];

// SPACING ////////////////////////////////
export const themeSpacing: any = {
  default: 12,
};

// SEO ///////////////////////////////////
export const variantMapping: any = {
  h1: "h3",
  h2: "h2",
  h3: "h3",
  h4: "h4",
  h5: "h5",
  h6: "h6",
  subtitle1: "h2",
  subtitle2: "h2",
  body1: "span",
  body2: "span",
};

export const themeConfig = {
  spacing: themeSpacing.default,
  hyphens: "manual",
  palette: {
    ...themeColors,
    colorSet: colorSet,
  },
  typography: {
    themeFontStyles: themeFontStyles,
    body1: {
      fontSize: "calc(1rem + 0.1em + 0.15vw)",
      lineHeight: 1.66,
    },
    body2: {
      fontSize: "calc(0.825rem + 0.1em + 0.15vw)",
      lineHeight: 1.66,
    },
    h1: {
      fontSize: "calc(1rem + 3em)",
      lineHeight: 1,
      letterSpacing: "0.02em",
      textTransform: "unset",
    },
    h2: {
      fontSize: "calc(1rem + 2.5em)",
      lineHeight: 1,
      letterSpacing: "0.02em",
      textTransform: "unset",
    },
    h3: {
      fontSize: "calc(1rem + 1.25em)",
      lineHeight: 1.1,
      letterSpacing: "0.02em",
      textTransform: "unset",
    },
    h4: {
      fontSize: "calc(1rem + 0.66em)",
      lineHeight: 1.3,
      letterSpacing: "0.02em",
      textTransform: "unset",
    },
    h5: {
      fontSize: "calc(1rem + 0.5em)",
      lineHeight: 1.2,
      letterSpacing: "0.02em",
      textTransform: "unset",
    },
    h6: {
      fontSize: "calc(1.1rem + 0.2em)",
      lineHeight: 1.2,
      letterSpacing: "0.02em",
      fontWeight: 400,
      textTransform: "unset",
    },
    subtitle1: {},
    subtitle2: {},
    caption: {
      fontSize: "calc(0.5rem + 0.1em + 0.15vw)",
      lineHeight: 1.33,
    },
    overline: {},
    button: {
      textTransform: "none",
      fontSize: "calc(0.875rem + 0.1em + 0.05vw)",
    },
  },
};

export const spacing = createSpacing(themeConfig.spacing);
export const typography = (key: any) => {
  return themeConfig.typography[key] as TypographyStyleOptions;
};
