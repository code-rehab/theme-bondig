var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import createSpacing from "@material-ui/core/styles/createSpacing";
// FONTS /////////////////////////////////////
export var themeFonts = {
    primary: "Domaine",
    secondary: "Domaine",
    tertiary: "Domaine",
};
// FONTSTYLES ///////////////////////////////
export var themeFontStyles = [
    {
        component: "h6",
        text: "Koptekst Groot",
        variant: "h1",
    },
    {
        component: "h1",
        text: "Koptekst",
        variant: "h1",
    },
    {
        component: "h2",
        text: "Subtitel",
        variant: "h2",
    },
    {
        component: "h3",
        text: "Introtekst",
        variant: "h3",
    },
    {
        component: "h4",
        text: "Sub-subtitel",
        variant: "h4",
    },
    {
        component: "h5",
        text: "Bijschrift",
        variant: "h5",
    },
    {
        component: "body1",
        text: "Paragraaf",
        variant: "body1",
    },
];
// COLORS ///////////////////////////////////
export var themeColors = {
    primary: {
        main: "#F85B00",
        light: "#FFECE1",
    },
    secondary: {
        main: "#A1D7DD",
        light: "#EDF7F9",
    },
    text: {
        main: "#4a4a4a",
    },
    white: "#fff",
};
// COLORPICKER ///////////////////////////////
export var colorSet = [
    themeColors.primary.main,
    themeColors.primary.light,
    themeColors.secondary.main,
    themeColors.secondary.light,
    themeColors.text.main,
];
// SPACING ////////////////////////////////
export var themeSpacing = {
    default: 12,
};
// SEO ///////////////////////////////////
export var variantMapping = {
    h1: "h3",
    h2: "h2",
    h3: "h3",
    h4: "h4",
    h5: "h5",
    h6: "h6",
    subtitle1: "h2",
    subtitle2: "h2",
    body1: "span",
    body2: "span",
};
export var themeConfig = {
    spacing: themeSpacing.default,
    hyphens: "manual",
    palette: __assign(__assign({}, themeColors), { colorSet: colorSet }),
    typography: {
        themeFontStyles: themeFontStyles,
        body1: {
            fontSize: "calc(1rem + 0.1em + 0.15vw)",
            lineHeight: 1.66,
        },
        body2: {
            fontSize: "calc(0.825rem + 0.1em + 0.15vw)",
            lineHeight: 1.66,
        },
        h1: {
            fontSize: "calc(1rem + 3em)",
            lineHeight: 1,
            letterSpacing: "0.02em",
            textTransform: "unset",
        },
        h2: {
            fontSize: "calc(1rem + 2.5em)",
            lineHeight: 1,
            letterSpacing: "0.02em",
            textTransform: "unset",
        },
        h3: {
            fontSize: "calc(1rem + 1.25em)",
            lineHeight: 1.1,
            letterSpacing: "0.02em",
            textTransform: "unset",
        },
        h4: {
            fontSize: "calc(1rem + 0.66em)",
            lineHeight: 1.3,
            letterSpacing: "0.02em",
            textTransform: "unset",
        },
        h5: {
            fontSize: "calc(1rem + 0.5em)",
            lineHeight: 1.2,
            letterSpacing: "0.02em",
            textTransform: "unset",
        },
        h6: {
            fontSize: "calc(1.1rem + 0.2em)",
            lineHeight: 1.2,
            letterSpacing: "0.02em",
            fontWeight: 400,
            textTransform: "unset",
        },
        subtitle1: {},
        subtitle2: {},
        caption: {
            fontSize: "calc(0.5rem + 0.1em + 0.15vw)",
            lineHeight: 1.33,
        },
        overline: {},
        button: {
            textTransform: "none",
            fontSize: "calc(0.875rem + 0.1em + 0.05vw)",
        },
    },
};
export var spacing = createSpacing(themeConfig.spacing);
export var typography = function (key) {
    return themeConfig.typography[key];
};
//# sourceMappingURL=config.js.map