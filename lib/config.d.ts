import { TypographyStyleOptions } from "@material-ui/core/styles/createTypography";
export declare const themeFonts: any;
export declare const themeFontStyles: any;
export declare const themeColors: any;
export declare const colorSet: any;
export declare const themeSpacing: any;
export declare const variantMapping: any;
export declare const themeConfig: {
    spacing: any;
    hyphens: string;
    palette: any;
    typography: {
        themeFontStyles: any;
        body1: {
            fontSize: string;
            lineHeight: number;
        };
        body2: {
            fontSize: string;
            lineHeight: number;
        };
        h1: {
            fontSize: string;
            lineHeight: number;
            letterSpacing: string;
            textTransform: string;
        };
        h2: {
            fontSize: string;
            lineHeight: number;
            letterSpacing: string;
            textTransform: string;
        };
        h3: {
            fontSize: string;
            lineHeight: number;
            letterSpacing: string;
            textTransform: string;
        };
        h4: {
            fontSize: string;
            lineHeight: number;
            letterSpacing: string;
            textTransform: string;
        };
        h5: {
            fontSize: string;
            lineHeight: number;
            letterSpacing: string;
            textTransform: string;
        };
        h6: {
            fontSize: string;
            lineHeight: number;
            letterSpacing: string;
            fontWeight: number;
            textTransform: string;
        };
        subtitle1: {};
        subtitle2: {};
        caption: {
            fontSize: string;
            lineHeight: number;
        };
        overline: {};
        button: {
            textTransform: string;
            fontSize: string;
        };
    };
};
export declare const spacing: import("@material-ui/core/styles/createSpacing").Spacing;
export declare const typography: (key: any) => TypographyStyleOptions;
