var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import { createMuiTheme } from "@material-ui/core";
import { themeConfig, typography, themeFonts } from "./config";
import "./src/style/fonts.css";
function createTheme(options) {
    return createMuiTheme(__assign({ spacing: themeConfig.spacing, typography: {
            themeFontStyles: themeConfig.typography.themeFontStyles,
            fontFamily: themeFonts.primary,
            allVariants: {},
            body1: __assign({}, typography("body1")),
            body2: __assign({}, typography("body2")),
            h1: __assign({}, typography("h1")),
            h2: __assign({}, typography("h2")),
            h3: __assign({}, typography("h3")),
            h4: __assign({}, typography("h4")),
            h5: __assign({}, typography("h5")),
            h6: __assign({}, typography("h6")),
            button: __assign({}, typography("button")),
            subtitle1: __assign({}, typography("subtitle1")),
            subtitle2: __assign({}, typography("subtitle2")),
            overline: __assign({}, typography("overline")),
            caption: __assign({}, typography("caption")),
        }, palette: __assign(__assign({}, themeConfig.palette), { editionBall: {
                color: "#e9550d",
                backgroundColor: "#ffc583",
                bottom: 12,
            }, error: {
                main: "#B94A48",
            } }), shape: {
            borderRadius: 0,
        }, props: {
            MuiCard: {
                elevation: 0,
            },
            MuiExpansionPanel: {
                elevation: 0,
            },
            MuiInputLabel: {
                shrink: true,
            },
            MuiTypography: {
                variant: "body2",
            },
        }, overrides: {
            MuiTypography: {
                paragraph: {
                    marginBottom: "1.66em",
                },
                gutterBottom: {
                    marginBottom: "0.5em",
                },
            },
            MuiButton: {
                outlined: {
                    borderRadius: 50,
                    paddingLeft: 36,
                    paddingRight: 36,
                },
            },
            MuiLink: {
                root: {
                    color: "inherit",
                },
            },
        } }, options));
}
var themeBondig = createTheme({});
export default themeBondig;
//# sourceMappingURL=theme.js.map